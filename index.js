/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	// console.log("Hello World");
	
	//first function here:
		function input_userDetails() {
			let full_name = prompt("Enter your fullname:"); 
			let location = prompt("Enter your location:");
			let age = prompt("Enter your age:");

			console.log("Hello, " + full_name); 			
			console.log("You are " + age + " years old.");
			console.log("You live in " + location);  
			
		}

		input_userDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
		function displayArtists() {
			let top_artist = ["Queen","The Beatles","Michael Jackson","Ben&Ben","Eraserheads"];
			console.log("1. " + top_artist[0]);
			console.log("2. " + top_artist[1]);
			console.log("3. " + top_artist[2]);
			console.log("4. " + top_artist[3]);
			console.log("5. " + top_artist[4]);

		}

		displayArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
		function displayMovies() {

				let top_movies = ["3 Idiots","Avengers : Endgame","Avengers : Infinity War","Hacksaw Ridge","Avatar"];
				let ratings = [100,94,,85,84,82];

				console.log("1. " + top_movies[0]);
				console.log("Rotten Tomatoes Rating: " + ratings[0] +"%");
				console.log("2. " + top_movies[1]);
				console.log("Rotten Tomatoes Rating: " + ratings[1] +"%");
				console.log("3. " + top_movies[2]);
				console.log("Rotten Tomatoes Rating: " + ratings[2] +"%");
				console.log("4. " + top_movies[3]);
				console.log("Rotten Tomatoes Rating: " + ratings[3] +"%");
				console.log("5. " + top_movies[4]);
				console.log("Rotten Tomatoes Rating: " + ratings[4] +"%");
		}

		displayMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);